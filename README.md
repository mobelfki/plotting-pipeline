Plotting Pipeline from MET Trigger Ntuple
==========================================

This repository contains a plotting pileline to produce different performance plots (histograms, efficiency curves, roc curvers ...) for the MET trigger studies. The code is adapted to ntuples produced by ``https://gitlab.cern.ch/jburr/masterperfstudies``

The code mainly based on different python modules:

    numpy, pandas, uproot, scipy, json and matplotlib

You might need to install the modules:

    pip instal --user model_name

The plotting code is configured from an external json file where you can define all the plots you want to produce.

How to run
----------

The code runs in two step

**1) Convert root Ntuples to pandas dataframe**
-------------------------------------------


    ./GetNumpy.sh

This runs the ``NTupleToNumpy`` module. This module converts root files to dataframe saved in HDF format. The dataframe are saved as ``data.DSID.h5`` where DSID is the data set ID given in the list of samples.

**Arguments**

    -t, treeName
    -s, list of samples to run over (.txt)
    -f, list of variables to save in the dataframe (.txt)
    -i, the input directory where root files are stored
    -o, the output directory where dataframe will be stored


**2) Plotting**
-----------


    ./plot.py -i inputdir -o outputdir -c configfile

**Arguments**

    -i, input directory where dataframe are saved
    -o, output directory where plots will be saved in pdf format
    -c, a json configuration file (example in ``Config``)

Plots are produced for all dataframe in the input direcotry and for multiple L1 trigger.

Json config file
-----------------

    Samples: a dic of the DSID of the samples you want to use (example {"000000": "sample1"})
    VarNames: variables map (example {"varX": "X"})
    Algos: map of HLT Trigger algorithm variable (met) (example {"Cell": "VarX"})
    AlgoName: map of the HLT Trigger algorithm name (example {"Cell": "HLT Cell"})
    L1Var: Level 1 Trigger MET variable (example "L1.MET")
    L1Unit: Unit of the variable "MeV" or "GeV"
    L1Cuts: L1 Trigger cuts to be used
    "hist": dic of histograms to be produced
    "stacks": dic of the histogram stacks to be produced
    "effsrate": dic of the total efficiency as function of HLT cut
    "eff": dic of the efficiency as function of a given variable (pile-up for example)
    "ROC": dic of ROC curves



