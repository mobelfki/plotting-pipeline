#!/bin/bash

inputdir=/afs/cern.ch/user/s/sabdelha/public/
outputdir=./data
samplesdir=/afs/cern.ch/work/m/mobelfki/TriggerML/Plotting/samples
featuresdir=/afs/cern.ch/work/m/mobelfki/TriggerML/Plotting/features
treeName=METTree
framework=jon

samples=(
	mc
	data
)

for sample in "${samples[@]}"
do
	echo ./NTupleToNumpy.py -t $treeName -s $samplesdir/samples_$framework'_'$sample.txt -i $inputdir -o $outputdir -f $featuresdir/features_$framework'_'$sample.txt
	./NTupleToNumpy.py -t $treeName -s $samplesdir/samples_$framework'_'$sample.txt -i $inputdir -o $outputdir -f $featuresdir/features_$framework'_'$sample.txt
done	
