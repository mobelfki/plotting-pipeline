#!/usr/bin/env python

# -*- coding: utf-8 -*-

__author__ = "BELFKIR Mohamed"
__email__ = "mohamed.belfkir@cern.ch"

import numpy as np
import pandas as pd
from argparse import ArgumentParser
import json
import os
seed = 215
import scipy.stats as stats
import numpy as np
np.random.seed(seed)
from numpy.random import seed as random_seed
random_seed(seed)
import matplotlib.pyplot as plt  
import math

def getArgs():
	
	args = ArgumentParser(description="Argumetns")
	args.add_argument('-i', '--input_dir', action='store', help='input directory')
	args.add_argument('-o', '--output_dir', action='store', help='output directory')
	args.add_argument('-c', '--plot_config', action='store', help='json config file for plots')
	
	config = json.load(open(str(args.parse_args().plot_config), 'r'))
	
	print(config.keys())
	
	for key in config.keys():
		if key in ['hist', 'effs', 'stacks', 'resolution', 'effsrate', 'ROC', 'ToPlot']: continue
		args.add_argument('--'+key, default=config[key])		
	for plot in config['ToPlot'].keys():
		for key, item in config[plot].items():		
			args.add_argument('--'+key, default=item)
			config['ToPlot'][plot].append(key)	
	args.add_argument('--plots', default=config['ToPlot'])
	
	return args
	
def getSamples(args, dsid):
	return args.Samples[str(dsid)]	
	
def getTag(args, tag):
	try: 
		return args.VarNames[tag]
	except:
		return tag
def getMETAlgo(args, tag):
	return args.Algos[tag]

def getAlgoName(args, tag):
	
	return args.AlgoName[tag]
	
def load_df(args):

	files  = os.listdir(args.parse_args().input_dir)
	print('--following result found :',files)
	dic = {}
	for file in files: 
		DSID = file.split('.')[1]
		dic.update({int(DSID): read_df_and_add_L1_Trigger(args, args.parse_args().input_dir+file)})

	return dic
	
def read_df_and_add_L1_Trigger(args, path):

	df = pd.read_hdf(path)
	L1 = df[args.parse_args().L1Var]
	if args.parse_args().L1Unit == 'MeV': L1 = L1 / 1000
	
	L1_Triggers = []
	for cut in args.parse_args().L1Cuts: 
		mask = L1 > cut
		df['L1_XE'+str(cut)] = mask
		L1_Triggers.append('L1_XE'+str(cut))
		
	try:
		args.add_argument('--L1_Triggers', default=L1_Triggers)
	except:
		pass	
	return df
		
def histVar(args, data, dsid, var, cut):
	
	plt.figure()
	values = data[var]
	weights = np.ones(values.shape[0], )
	if args.UseWeights: weights = data.Weight
	if vars(args)[var]['unit'] == 'MeV': values = values / 1000
	plt.hist(values, bins=vars(args)[var]['bins'], range=vars(args)[var]['range'], density=vars(args)[var]['density'], histtype=vars(args)[var]['type'], weights=weights)
	plt.xlabel(vars(args)[var]['xlabel'])
	plt.ylabel(vars(args)[var]['ylabel'])
	legend = plt.legend([getTag(args, var)])
	legend.set_title("%s, L1: %s"%(getSamples(args, dsid), getTag(args, cut)))
	plt.savefig('%s/plots/var_%s_sample_%s_Cut_%s.pdf'%(args.output_dir, var, getSamples(args, dsid), cut))
	plt.close()

def hist(args, data, var):
	
	for key, item in data.items():
		if key == 999999 and 'Truth' in var: continue	
		histVar(args, item, key, var, 'NONE')
		for L1 in args.L1_Triggers:
			mask = item[L1] == 1
			histVar(args, item[mask], key, var, L1)	

def stackVars(args, data, dsid, stack, mask, cut):
	
	plt.figure()
	hists = vars(args)[stack]['vars']
	for hist in hists:
		if dsid == 999999 and 'Truth' in hist: continue
		values = data[hist][mask]
		weights = np.ones(values.shape[0], )
		if args.UseWeights: weights = data.Weight[mask]
		
		if vars(args)[stack]['unit'] == 'MeV': values = values / 1000
		plt.hist(values, bins=vars(args)[stack]['bins'], range=vars(args)[stack]['range'], density=vars(args)[stack]['density'], histtype=vars(args)[stack]['type'], weights=weights)
	plt.xlabel(vars(args)[stack]['xlabel'])
	plt.ylabel(vars(args)[stack]['ylabel'])
	legend = plt.legend([ getTag(args, tag) for tag in vars(args)[stack]['vars']])
	legend.set_title("%s, L1: %s"%(getSamples(args, dsid), getTag(args, cut)))
	plt.savefig('%s/plots/Stack_%s_sample_%s_Cut_%s.pdf'%(args.output_dir, vars(args)[stack]['name'], getSamples(args, dsid), cut))
	plt.close()
	
def stacks(args, data, stack):

	for key, item in data.items():
		mask = np.ones(item.shape[0],) == 1
		stackVars(args, item, key, stack, mask, 'NONE')
		for L1 in args.L1_Triggers: 
			mask = item[L1] == 1
			stackVars(args, item, key, stack, mask, L1) 

def resoVars(args, data, dsid, reso, mask, cut):

	plt.figure()
	hists = vars(args)[reso]['vars']
	for hist in hists:
		if dsid == 999999: continue
		values = data[hist][mask]
		weights = np.ones(values.shape[0], )
		if args.UseWeights: weights= data.Weight[mask]
		ref = data[vars(args)[reso]['ref']][mask]
		values = (values - ref)/ref
		plt.hist(values, bins=vars(args)[reso]['bins'], range=vars(args)[reso]['range'], density=vars(args)[reso]['density'], histtype=vars(args)[reso]['type'], weights=weights)
	plt.xlabel(vars(args)[reso]['xlabel'])
	plt.ylabel(vars(args)[reso]['ylabel'])
	legend = plt.legend([ getTag(args, tag) for tag in vars(args)[reso]['vars']])
	legend.set_title("%s, L1: %s"%(getSamples(args, dsid), getTag(args, cut)))
	plt.savefig('%s/plots/Resolution_%s_sample_%s_Cut_%s.pdf'%(args.output_dir, vars(args)[reso]['name'], getSamples(args, dsid), cut))
	plt.close()
	
def getBinContents(data, weights, args, eff):
	
	plt.figure()
	content, bins, _ = plt.hist(data, bins=vars(args)[eff]['bins'], range=vars(args)[eff]['range'], density=vars(args)[eff]['density'], histtype=vars(args)[eff]['type'], weights=weights)
	plt.close()
	return content, bins
	
def getEfficiencies(args, values, weights, eff):

	Range  = vars(args)[eff]['range']
	nbin   = vars(args)[eff]['bins']
	cuts   = np.linspace(Range[0], Range[1], num=nbin, endpoint=False)
	n0     = float(values.shape[0])
	n02    = n0*n0
	if args.UseWeights:
		n0  = weights.sum()
		n02 = (weights*weights).sum()
		
	rate = []
	low  = []
	high = []
	for cut in cuts:
		ispassed = values >= cut
		npassed = float(ispassed.sum()) # this should corrected when adding weights
		npassed2= npassed*npassed
		if args.UseWeights:
			npassed = weights[ispassed].sum()
			npassed2= (weights[ispassed]*weights[ispassed]).sum()
		
		f, l, h = computeSingleEff(n0, n02, npassed, npassed2, args.UseWeights)
		rate.append(f)
		low.append(l)
		high.append(h)
	
	return cuts, rate, low, high		
	
def effVars(args, data, dsid, eff, mask, cut):
	
	plt.figure()
	hists = vars(args)[eff]['vars']
	for hist in hists:
		if dsid == 999999 and 'Truth' in vars(args)[eff]['truth_var']: continue
		truth_mask = data[vars(args)[eff]['truth_var']] > vars(args)[eff]['truth_cut']	
		mask = mask & truth_mask
		ref    = data[vars(args)[eff]['ref']][mask]
		values = data[getMETAlgo(args, hist)][mask]
		weights = np.ones(values.shape[0], )
		if args.UseWeights: weights= data.Weight[mask]
		
		if vars(args)[eff]['Varunit'] == 'MeV': values = values / 1000
		if vars(args)[eff]['Refunit'] == 'MeV': ref = ref / 1000
		all_value = values > 0
		passed    = values > vars(args)[eff]['cut']
		n_all, bin_all = getBinContents(ref[all_value], weights[all_value], args, eff) 
		n_passed, bin_pass = getBinContents(ref[passed], weights[passed], args, eff)
		bins, effs, eff_err_l, eff_err_h = computeEff(n_all, n_passed, bin_pass)
		plt.errorbar(bins, effs, yerr= [eff_err_l, eff_err_h], fmt='o', markersize=3)
		
	plt.xlabel(vars(args)[eff]['xlabel'])
	if dsid != 999999: plt.ylabel("%s (Truth MET > %s)"%(vars(args)[eff]['ylabel'], vars(args)[eff]['truth_cut']))
	if dsid == 999999: plt.ylabel("%s "%(vars(args)[eff]['ylabel']))
	legend = plt.legend([getAlgoName(args, tag) for tag in vars(args)[eff]['vars']])
	legend.set_title("%s, L1: %s, %s > %s"%(getSamples(args, dsid) , getTag(args, cut), vars(args)[eff]['cutName'], str(vars(args)[eff]['cut'])))
	plt.savefig('%s/plots/Efficiency_%s_sample_%s_L1Cut_%s_HLTCut_%s_TruthMETCut_%s.pdf'%(args.output_dir, vars(args)[eff]['name'], getSamples(args, dsid), cut, vars(args)[eff]['cut'], vars(args)[eff]['truth_cut']))
	plt.close()

	
	
def effRateVars(args, data, dsid, eff, mask, cut):

	plt.figure()
	hists = vars(args)[eff]['vars']
	for hist in hists: 
			if dsid != 999999: truth_mask = data[vars(args)[eff]['truth_var']] > vars(args)[eff]['truth_cut']
			if dsid == 999999: truth_mask = np.ones(mask.shape[0],) == 1	
			mask = mask & truth_mask
			values = data[getMETAlgo(args, hist)][mask]
			weights = np.ones(values.shape[0], )
			if args.UseWeights: weights= data.Weight[mask]
			if vars(args)[eff]['Varunit'] == 'MeV': values = values / 1000
			cuts, rate, low, high = getEfficiencies(args, values, weights, eff)
			plt.errorbar(cuts, rate, yerr=[low, high], fmt='o', markersize=3)
	plt.xlabel(vars(args)[eff]['xlabel'])
	if dsid != 999999: plt.ylabel("%s (Truth MET > %s)"%(vars(args)[eff]['ylabel'], vars(args)[eff]['truth_cut']))
	if dsid == 999999: plt.ylabel("%s "%(vars(args)[eff]['ylabel']))
	legend = plt.legend([getAlgoName(args, tag) for tag in vars(args)[eff]['vars']])
	legend.set_title("%s, L1: %s"%(getSamples(args, dsid) ,getTag(args, cut)))
	plt.savefig('%s/plots/EfficiencyRate_%s_sample_%s_L1Cut_%s_TruthMETCut_%s.pdf'%(args.output_dir, vars(args)[eff]['name'], getSamples(args, dsid), cut, vars(args)[eff]['truth_cut']))
	plt.close()

def rocVars(args, df_sig, df_bkg, sig, bkg, roc, mask_sig, mask_bkg, cut):
	
	plt.figure()
	hists = vars(args)[roc]['vars']
	for hist in hists:
		if sig != 999999: truth_mask_sig = df_sig[vars(args)[roc]['truth_var']] > vars(args)[roc]['sig_truth_cut']
		if bkg != 999999: truth_mask_bkg = df_bkg[vars(args)[roc]['truth_var']] > vars(args)[roc]['bkg_truth_cut']
		if sig == 999999: truth_mask_sig = np.ones(mask_sig.shape[0],) == 1	
		if bkg == 999999: truth_mask_bkg = np.ones(mask_bkg.shape[0],) == 1	
		
		mask_sig = mask_sig & truth_mask_sig
		mask_bkg = mask_bkg & truth_mask_bkg
		val_sig  = df_sig[getMETAlgo(args, hist)][mask_sig]
		val_bkg  = df_bkg[getMETAlgo(args, hist)][mask_bkg]
		wgt_sig  = np.ones(val_sig.shape[0], )
		wgt_bkg  = np.ones(val_bkg.shape[0], ) 
		if args.UseWeights:
			wgt_sig = df_sig.Weight[mask_sig]
			wgt_bkg = df_bkg.Weight[mask_bkg]
		if vars(args)[roc]['Varunit'] == 'MeV': val_sig = val_sig / 1000
		if vars(args)[roc]['Varunit'] == 'MeV': val_bkg = val_bkg / 1000
		cuts_sig, rate_sig, low_sig, high_sig = getEfficiencies(args, val_sig, wgt_sig, roc)
		cuts_bkg, rate_bkg, low_bkg, high_bkg = getEfficiencies(args, val_bkg, wgt_bkg, roc)	
		plt.errorbar(rate_sig, rate_bkg, fmt='-', markersize=3)
	plt.xlabel("%s (Truth MET > %s)"%(vars(args)[roc]['xlabel'], vars(args)[roc]['sig_truth_cut']))
	plt.ylabel(vars(args)[roc]['ylabel'])
	plt.xlim([0.75, 1.0])
	plt.yscale('log')
	legend = plt.legend([getAlgoName(args, tag) for tag in vars(args)[roc]['vars']])
	legend.set_title("%s vs %s, L1: %s"%(getSamples(args, sig), getSamples(args, bkg) ,getTag(args, cut)))
	plt.savefig('%s/plots/ROC_%s_Sig_%s_Bkg_%s_L1Cut_%s_SigTruthMETCut_%s_BkgTruthMETCut_%s.pdf'%(args.output_dir, vars(args)[roc]['name'], getSamples(args, sig), getSamples(args, bkg), cut, vars(args)[roc]['sig_truth_cut'],  vars(args)[roc]['bkg_truth_cut']))
	plt.close()		
			
def resos(args, data, reso):
	
	for key, item in data.items():
		mask = np.ones(item.shape[0],) == 1
		resoVars(args, item, key, reso, mask, 'NONE')
		for L1 in args.L1_Triggers:
			mask = item[L1] == 1
			resoVars(args, item, key, reso, mask, L1)
						
def effs(args, data, eff):

	for key, item in data.items():
		mask = np.ones(item.shape[0],) == 1
		effVars(args, item, key, eff, mask, 'NONE')
		
		for L1 in args.L1_Triggers:
			mask = item[L1] == 1
			effVars(args, item, key, eff, mask, L1)
			
def effsrate(args, data, eff):
	
	for key, item in data.items():
		mask = np.ones(item.shape[0],) == 1
		effRateVars(args, item, key, eff, mask, 'NONE')
		for L1 in args.L1_Triggers:
			mask = item[L1] == 1
			effRateVars(args, item, key, eff, mask, L1)
			
def rocs(args, data, roc): 
	
	sig = vars(args)[roc]['sig']
	bkg = vars(args)[roc]['bkg']
	df_sig = data[sig]
	df_bkg = data[bkg]
	mask_sig = np.ones(df_sig.shape[0],) == 1				
	mask_bkg = np.ones(df_bkg.shape[0],) == 1
	rocVars(args, df_sig, df_bkg, sig, bkg, roc, mask_sig, mask_bkg, 'NONE')
	
	for L1 in args.L1_Triggers:
		mask_sig = df_sig[L1] == 1
		mask_bkg = df_bkg[L1] == 1
		rocVars(args, df_sig, df_bkg, sig, bkg, roc, mask_sig, mask_bkg, L1)
	
	
def computeEff(n_all, n_passed, bins):

	effs = np.array(n_passed/n_all, dtype=float)
	eff_err_l = []
	eff_err_h = []
	
	confLevel = 0.683
	for n_a, n_p in zip(n_all, n_passed):
		l, h = getEffError(n_a, n_p)
		eff_err_l.append(l)
		eff_err_h.append(h)
	
	bin_center = 0.5*(bins[1:]+bins[:-1])
	
	return bin_center, effs, np.array(eff_err_l), np.array(eff_err_h)
	
def getEffError(n_all, n_all2, n_passed, n_passed2, UseWeights, confLevel = 0.683):
	l = 0
	h = 0
	eff = float(n_passed/n_all)
	if n_passed == n_all:
		h = 1.0
	else:
		if not UseWeights:
			h = stats.beta.ppf(confLevel + 0.5*(1-confLevel), n_passed+1., n_all-n_passed)
		else:
			v = math.sqrt((n_passed2 * (1. - 2*eff) + n_all2 * eff * eff) / (n_all * n_all))
			delta = stats.norm.ppf( 0.5*(1+confLevel), 0, v)
			h = delta
			if eff - delta < 0: h = eff

	if n_passed == 0:
		l = 0
	else:
		if not UseWeights:
			l = stats.beta.ppf(0.5*(1+confLevel) - confLevel, n_passed, n_all-n_passed+1.)
		else:
			v = math.sqrt((n_passed2 * (1. - 2*eff) + n_all2 * eff * eff) / (n_all * n_all))
			delta = stats.norm.ppf( 0.5*(1+confLevel), 0, v)
			l = delta
			if eff + delta > 1: h = 1-eff
			
	errLow = eff-l 
	errHigh= h-eff
	if UseWeights:
		errLow = l
		errHigh= h		
	
	return errLow, errHigh	
	

def computeSingleEff(n_all, n_all2, n_passed, n_passed2, UseWeights):

	effs = float(n_passed/n_all)
	l, h = getEffError(n_all, n_all2, n_passed, n_passed2, UseWeights)
	return effs, l, h
		
def main():

	args = getArgs()
	try:
		os.makedirs(args.parse_args().output_dir+"/plots/")
	except:
		print('dir ' + args.parse_args().output_dir + ' already exists')

	df_dic  = load_df(args)
	args = args.parse_args()
	
	for key in args.plots.keys():
		print(key)
		
		if key == 'hist':
			print('hist: %s'%(args.plots[key]))
			for var in args.plots[key]:
				hist(args, df_dic, var)
		
		if key == 'stacks':
			print('stacks: %s'%(args.plots[key]))
			for stack in args.plots[key]:
				stacks(args, df_dic, stack)
		
						
		if key == 'resolution':
			print('resolution: %s'%(args.plots[key]))
			for reso in args.plots[key]:
				resos(args, df_dic, reso)
		if key == 'effs':
			print('effs: %s'%(args.plots[key]))
			for eff in args.plots[key]:
				effs(args, df_dic, eff)
						
		if key == 'effsrate':
			print('effs: %s'%(args.plots[key]))
			for eff in args.plots[key]:
				effsrate(args, df_dic, eff)
		
		if key == 'ROC':
			print('ROCs: %s'%(args.plots[key]))
			for roc in args.plots[key]:
				rocs(args, df_dic, roc)					
		

if __name__ == '__main__':
	
	main()		
