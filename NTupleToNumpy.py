#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "BELFKIR Mohamed"
__email__ = "mohamed.belfkir@cern.ch"

import ROOT 
import uproot as pr
import pandas as pd
import numpy as np
from math import pi as PI
import math
from argparse import ArgumentParser
import sys

def getArgs():
	"""
	Get arguments from command line.
	"""
	args = ArgumentParser(description="Argumetns for NTupleToNumpy for Plotting")
	args.add_argument('-t', '--treeName', action='store', default='trigMetTree', help='Tree name')
	args.add_argument('-s', '--samples', action='store', required=True, help='Samples name to process .txt splitted by ')
	args.add_argument('-i', '--inputdir', action='store', default='/eos/user/m/mobelfki/TriggerSamples/', help='Input Directory')
	args.add_argument('-f', '--features', action='store', required=True, help='Features list')
	args.add_argument('-o', '--outdir', action='store', default='data', help='Output Directory')
	return args.parse_args()

class NTupleToNumpy:
	"""
	Produce numpy arrays form NTuple class 
	The output is saved in Array/output directory
	Doesn't take any inputs 
	"""

	def __init__(self, name):
		self.name = name;
	
	def loadTree(self, path, sample, tree):
		self.Tree = pr.open(path+'/'+sample)[tree]
		
	def loadFeatures(self, txt):
		with open(txt,'r') as features_file:
			self.Features = features_file.read().splitlines();
		features_file.close();

	def Execute(self):
		
		df = pd.DataFrame(self.Tree.arrays(self.Features, library="pd"));
		
		try:
			df['Weight'] = df['weight_mcWeight']
		except:
			pass
		try:
			df['Weight'] = df['MCEventWeight']
		except:
			pass
		
		try:
			df['Weight'] = df['EBWeight']	
		except:
			pass
		
		return df;

def main():
	"""
	The main function of NTupleToNumpy
	"""
	args=getArgs()
	with open(args.samples,'r') as samples_file:
		samples_name = samples_file.read().splitlines();
	samples_file.close();
	features_file = args.features
	
	for samples in samples_name:
		out = []
		samples = samples.split(" ")
		for sample in samples[:-1]:
			print(sample)
			NTuple2Numpy = NTupleToNumpy(sample);
			NTuple2Numpy.loadTree(args.inputdir, sample, args.treeName);
			NTuple2Numpy.loadFeatures(features_file);
			label = np.array([0]);
			out.append(NTuple2Numpy.Execute());
		df = out[0]	
		for i in range(1, len(out)):
			df = df.append(out[i], ignore_index=True)	
		df.to_hdf(args.outdir+'/data.'+samples[-1]+'.h5', key='df', mode='w')		
		
if __name__ == '__main__':
	main()
		
